New-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\services\TCPIP6\Parameters -Name DisabledComponents -PropertyType DWord -Value 0xffffffff
New-NetLBFOTeam -Name HVTeam -TeamMembers Ethernet* -TeamingMode SwitchIndependent -LoadBalancingAlgorithm HyperVPort -Confirm:$false
New-NetIPAddress -InterfaceAlias HVTeam -IPAddress 192.168.120.199 -PrefixLength 24 -DefaultGateway 192.168.120.1
Set-DNSClientServerAddress -InterfaceAlias HVTeam -ServerAddress 192.168.120.10,192.18.120.11,192.168.120.12

Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False
Enable-PSRemoting -Confirm:$false

$djUserName = "ACME-CORP\Administrator"
$djPassword = ConvertTo-SecureString "Password1" -AsPlainText -Force
#$djPassword = ConvertTo-SecureString "01000000d08c9ddf0115d1118c7a00c04fc297eb01000000dd53c739b0100740b65784a564178edc0000000002000000000010660000000100002000000047294de0d559d3801f35b35d3423c679cdac2da8893b81ed07baa9f534a481a3000000000e8000000002000020000000236285b1c2fe6e3c988ad9fed7f1701545278b0d6a7ed375cf7506c6cea306c420000000d9216b2902ec25d695433b72a2f5e7f6d629e0835a40513bbd8afb608fbbf1d4400000001efe2a365e948ac93ccab1de97c1c5b5d29849afc527f9a7bd582c55c6adbd1e7975cac79de6578f7b1fc6444255f444f38076afc7ece2458836ac6a5580e2eb"
$dcCred = New-Object System.Management.Automation.PSCredential $djUserName, $djPassword
Add-Computer -DomainName acme-corp.local -Credential $dcCred -ComputerName . -OUPath "OU=Europe,OU=Global Operations,DC=acme-corp,DC=local"
Rename-Computer -NewName client66 -ComputerName . -DomainCredential $dcCred -Confirm:$false
