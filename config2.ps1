﻿function report($message) {
  Write-Host $message;
}

function configureComputer($config) {
  report "Configuration for sever: ";
  report $config;
  report "Disabling IPv6.";
  New-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\services\TCPIP6\Parameters -Name DisabledComponents -PropertyType DWord -Value 0xffffffff;
  report "Building network team.";
  New-NetLBFOTeam -Name $config.LBFOTeamName -TeamMembers $config.LBFOTeamMembers -TeamingMode $config.LBFOTeamingMode -LoadBalancingAlgorithm $config.LBFOLoadBalancingAlgorithm -Confirm:$false;
  report "Waiting 30 seconds for the network."
  Start-Sleep -s 30;
  report "Configuring IP address.";
  New-NetIPAddress -InterfaceAlias $config.LBFOTeamName -IPAddress $config.IPAddress -PrefixLength $config.IPPrefixLength -DefaultGateway $config.IPDefaultGateway;
  report "Configuring DNS client settings.";
  Set-DNSClientServerAddress -InterfaceAlias $config.LBFOTeamName -ServerAddress $config.DNSServers;

  report "Disabling firewall profiles.";
  Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False;
  report "Enabling PowerShell Remoting.";
  Enable-PSRemoting -Confirm:$false;
  report "Enabling remote desktop.";
  cscript C:\Windows\System32\scregedit.wsf /ar 0;
  cscript C:\Windows\System32\scregedit.wsf /cs 0
  report "Setting time-zone.";
  tzutil /s $config.TimeZone;
  report "Waiting 30 seconds for the network."
  Start-Sleep -s 10;

  report "Joining computer to the domain.";
  $djUserName = $config.DomainJoinUserName;
  $djPassword = ConvertTo-SecureString $config.DomainJoinPassword -AsPlainText -Force
  #$djPassword = ConvertTo-SecureString "01000000d08c9ddf0115d1118c7a00c04fc297eb01000000dd53c739b0100740b65784a564178edc0000000002000000000010660000000100002000000047294de0d559d3801f35b35d3423c679cdac2da8893b81ed07baa9f534a481a3000000000e8000000002000020000000236285b1c2fe6e3c988ad9fed7f1701545278b0d6a7ed375cf7506c6cea306c420000000d9216b2902ec25d695433b72a2f5e7f6d629e0835a40513bbd8afb608fbbf1d4400000001efe2a365e948ac93ccab1de97c1c5b5d29849afc527f9a7bd582c55c6adbd1e7975cac79de6578f7b1fc6444255f444f38076afc7ece2458836ac6a5580e2eb"
  $dcCred = New-Object System.Management.Automation.PSCredential $djUserName, $djPassword
  Add-Computer -DomainName $config.DomainJoinDomainName -Credential $dcCred -ComputerName . -OUPath $config.DomainJoinOUPath;
  Rename-Computer -NewName $config.ComputerName -ComputerName . -DomainCredential $dcCred -Confirm:$false

  report "Rebooting.";
  shutdown /r /t 60 /c "Configuration complete.  Reboot in a minute.";
}

$networkPath = "\\vmware-host\Shared Folders\C\TR4";
$fileName = "data.csv";
net use Z: $networkPath /persistent:no;

# Read data from file.
$data = Import-CSV ($networkPath + "\" + $fileName);

# System's serial number.
$sysSerialNumber = (Get-WMIObject Win32_BIOS).SerialNumber;

$sysFound = $false;

ForEach ($item in $data) {
  if ($item.SerialNumber -eq $sysSerialNumber -and (!$sysFound)) {
    $sysFound = $true;
    configureComputer $item;
  }
}