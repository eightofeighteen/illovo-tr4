function report($message) {
  Write-Host $message;
}

function configureComputer($config) {
  #Write-Host "Renaming adapters for test.";
  #Rename-NetAdapter "Ethernet" -NewName "MGMT1"
  #Rename-NetAdapter "Ethernet 2" -NewName "MGMT2"
  #Rename-NetAdapter "Ethernet 3" -NewName "LM1"
  #Rename-NetAdapter "Ethernet 4" -NewName "LM2"
  #Rename-NetAdapter "Ethernet 5" -NewName "VM1"
  #Rename-NetAdapter "Ethernet 6" -NewName "VM2"
  Write-Host "ConfigureComputer called.";
  report "Configuration for sever: ";
  report $config;
  report "Disabling IPv6.";
  New-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\services\TCPIP6\Parameters -Name DisabledComponents -PropertyType DWord -Value 0xffffffff;
  report "Building network team.";
  #New-NetLBFOTeam -Name $config.LBFOTeamName -TeamMembers $config.LBFOTeamMembers -TeamingMode $config.LBFOTeamingMode -LoadBalancingAlgorithm $config.LBFOLoadBalancingAlgorithm -Confirm:$false;
  New-NetLBFOTeam -Name $config.MGMT_TeamName -TeamMembers $config.MGMT_TeamMembers -TeamingMode $config.MGMT_TeamingMode -LoadBalancingAlgorithm $config.MGMT_LoadBalancingAlgorithm -Confirm:$false;
  New-NetLBFOTeam -Name $config.LM_TeamName -TeamMembers $config.LM_TeamMembers -TeamingMode $config.LM_TeamingMode -LoadBalancingAlgorithm $config.LM_LoadBalancingAlgorithm -Confirm:$false;
  New-NetLBFOTeam -Name $config.VM_TeamName -TeamMembers $config.VM_TeamMembers -TeamingMode $config.VM_TeamingMode -LoadBalancingAlgorithm IPAddresses -Confirm:$false; #Change load balancing algorithm.


  report "Waiting 10 seconds for the network."
  Start-Sleep -s 10;
  New-VMSwitch "VSwitch1" -NetAdapterName $config.VM_TeamName -AllowManagementOS $false;
  report "Configuring IP address.";
  #New-NetIPAddress -InterfaceAlias $config.LBFOTeamName -IPAddress $config.IPAddress -PrefixLength $config.IPPrefixLength -DefaultGateway $config.IPDefaultGateway;
  New-NetIPAddress -InterfaceAlias $config.MGMT_TeamName -IPAddress $config.MGMT_IPAddress -AddressFamily IPv4 -PrefixLength $config.MGMT_IPPrefixLength -DefaultGateway $config.MGMT_IPDefaultGateway;
  New-NetIPAddress -InterfaceAlias $config.LM_TeamName -IPAddress $config.LM_IPAddress -AddressFamily IPv4 -PrefixLength $config.LM_IPPrefixLength;

  report "Configuring DNS client settings.";
  #Set-DNSClientServerAddress -InterfaceAlias $config.LBFOTeamName -ServerAddress $config.DNSServers;
  Set-DNSClientServerAddress -InterfaceAlias $config.MGMT_TeamName -ServerAddress $config.MGMT_DNSServer;
  $idx = (Get-WmiObject Win32_NetworkAdapter | Where {$_.NetConnectionID -eq $config.LM_TeamName}).InterfaceIndex;
  (Get-WmiObject Win32_NetworkAdapterConfiguration | Where {$_.InterfaceIndex -eq $idx}).SetDynamicDNSRegistration($false);

  report "Disabling firewall profiles.";
  Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False;
  report "Enabling PowerShell Remoting.";
  Enable-PSRemoting -Confirm:$false;
  Set-ExecutionPolicy RemoteSigned -Force;
  report "Enabling remote desktop.";
  cscript C:\Windows\System32\scregedit.wsf /ar 0;
  cscript C:\Windows\System32\scregedit.wsf /cs 0
  report "Setting time-zone.";
  tzutil /s $config.TimeZone;
  Import-Module C:\Windows\Setup\Scripts\AdjustVirtualMemoryPagingFileSize.psm1;
  Set-OSCVirtualMemory -InitialSize 10240 -MaximumSize 10240 -DriveLetter "C:";
  report "Waiting 10 seconds for the network."
  Start-Sleep -s 10;

  report "Joining computer to the domain.";
  $djUserName = $config.DomainJoinUserName;
  $djPassword = ConvertTo-SecureString $config.DomainJoinPassword -AsPlainText -Force
  #$djPassword = ConvertTo-SecureString "01000000d08c9ddf0115d1118c7a00c04fc297eb01000000dd53c739b0100740b65784a564178edc0000000002000000000010660000000100002000000047294de0d559d3801f35b35d3423c679cdac2da8893b81ed07baa9f534a481a3000000000e8000000002000020000000236285b1c2fe6e3c988ad9fed7f1701545278b0d6a7ed375cf7506c6cea306c420000000d9216b2902ec25d695433b72a2f5e7f6d629e0835a40513bbd8afb608fbbf1d4400000001efe2a365e948ac93ccab1de97c1c5b5d29849afc527f9a7bd582c55c6adbd1e7975cac79de6578f7b1fc6444255f444f38076afc7ece2458836ac6a5580e2eb"
  $dcCred = New-Object System.Management.Automation.PSCredential $djUserName, $djPassword
  Add-Computer -DomainName $config.DomainJoinDomainName -Credential $dcCred -ComputerName . -OUPath $config.DomainJoinOUPath;
  Start-Sleep -s 10;
  Rename-Computer -NewName $config.ComputerName -ComputerName . -DomainCredential $dcCred -Confirm:$false

  REG ADD HKLM\Software\Microsoft\Windows\CurrentVersion\RunOnce /v SetupCont /t REG_SZ /d "c:\windows\setup\scripts\report-wrapper.cmd"

  report "Rebooting.";
  shutdown /r /t 60 /c "Configuration complete.  Reboot in a minute.";
  Read-Host;
}

$defLoc = "data.csv";
$userLoc = "";
Write-Host "Before running through this script, rename the network adapters."
Write-Host ("Enter the configuration file [" + $defLoc + "]: ") -NoNewLine;
$userLoc = Read-Host;
if ($userLoc -eq "") {
    $userLoc = $defLoc;
}
while ($userLoc -eq "" -or (Test-Path $userLoc) -eq $false) {
    Write-Host ("Unable to open '" + $userLoc + "'.");
    Write-Host ("Enter the configuration file [" + $defLoc + "]: ") -NoNewLine;
    $userLoc = Read-Host;
    if ($userLoc -eq "") {
        $userLoc = $defLoc;
    }
}
Write-Host ("Looking in " + $userLoc);

$data = Import-CSV $userLoc;
$sysSerialNumber = (Get-WMIObject Win32_BIOS).SerialNumber.TrimEnd();

$sysFound = $false;
ForEach ($item in $data) {
  if ($item.SerialNumber -eq $sysSerialNumber -and (!$sysFound)) {
    Write-Host "Found host.";
    $sysFound = $true;
    configureComputer $item;
  }
}

if ($sysFound -eq $false) {
    Write-Host ("Unable to find serial number " + $sysSerialNumber.TrimEnd() + " in the configuration file.  Please check that the serial number exists in the configuration file and try again.");
} else {
    Write-Host ("Success.  Check the log file to verify configuration.");
}
